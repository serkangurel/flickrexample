package com.serkangurel.flickrexample.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import com.serkangurel.flickrexample.R
import ru.alexbykov.nopaginate.callback.OnRepeatListener
import ru.alexbykov.nopaginate.item.ErrorItem

class CustomErrorItem(private val listener: OnRepeatClickListener) : ErrorItem {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_error, parent, false)
        return object : RecyclerView.ViewHolder(view) {
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, p1: Int, p2: OnRepeatListener?) {
        val btnRepeat = holder?.itemView?.findViewById<Button>(R.id.btnRepeat)
        btnRepeat?.setOnClickListener {
            listener.onRepeatClick()
        }
    }
}