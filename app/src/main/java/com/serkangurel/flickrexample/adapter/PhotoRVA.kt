package com.serkangurel.flickrexample.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.serkangurel.flickrexample.R
import com.serkangurel.flickrexample.data.model.Photo
import com.serkangurel.flickrexample.ui.photo.PhotoActivity


class PhotoRVA(context: Context, list: List<Photo>? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val photoList = arrayListOf<Photo>()
    private var context: Context

    init {
        list?.let { photoList.addAll(it) }
        this.context = context
    }

    fun setList(list: List<Photo>?) {
        if (list != null) {
            photoList.clear()
            photoList.addAll(list)
            notifyDataSetChanged()
        }
    }

    fun addList(list: List<Photo>?) {
        if (list != null) {
            val pos = itemCount
            photoList.addAll(list)
            notifyItemRangeInserted(pos, list.size)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_photo, parent, false)
        return PhotoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return photoList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val photoViewHolder = holder as PhotoViewHolder
        val photo = photoList.get(position)

        Glide.with(context)
            .load(photo.urlM)
            .into(photoViewHolder.ivPhoto)

        photoViewHolder.tvTitle.text = photo.title
        photoViewHolder.tvDate.text = photo.date

        photoViewHolder.itemView.setOnClickListener {
            PhotoActivity.start(context, photo)
        }
    }

    inner class PhotoViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var tvTitle: TextView
        var ivPhoto: ImageView
        var tvDate: TextView

        init {
            tvTitle = view.findViewById(R.id.tv_title)
            ivPhoto = view.findViewById(R.id.iv_photo)
            tvDate = view.findViewById(R.id.tv_date)
        }
    }
}