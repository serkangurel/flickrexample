package com.serkangurel.flickrexample.adapter

interface OnRepeatClickListener {
    fun onRepeatClick()
}