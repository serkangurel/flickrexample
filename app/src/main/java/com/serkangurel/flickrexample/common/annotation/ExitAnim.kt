package com.serkangurel.flickrexample.common.annotation

import androidx.annotation.AnimRes

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
annotation class ExitAnim(

        @AnimRes
        val enterAnim: Int,

        @AnimRes
        val exitAnim: Int

)