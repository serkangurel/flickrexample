package com.serkangurel.flickrexample.common.extensions

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.text.TextUtils
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

inline fun <reified T : Fragment> newFragmentInstance(noinline init: Bundle.() -> Unit = {}): T {
    return T::class.java.newInstance().apply {
        val bundle = Bundle().apply(init)
        if (!bundle.isEmpty) arguments = bundle
    }
}

inline fun <reified T : Any> Activity.startActivity(
        requestCode: Int = -1,
        options: Bundle? = null,
        noinline init: Intent.() -> Unit = {}) {

    val intent = newIntent<T>(this)
    intent.init()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        if (requestCode > -1)
            startActivityForResult(intent, requestCode, options)
        else
            startActivity(intent, options)
    } else {
        if (requestCode > -1)
            startActivityForResult(intent, requestCode)
        else
            startActivity(intent)
    }
}

inline fun <reified T : Any> Fragment.startActivity(
        requestCode: Int = -1,
        options: Bundle? = null,
        noinline init: Intent.() -> Unit = {}) {

    if (context == null) return

    val intent = newIntent<T>(context!!)
    intent.init()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        if (requestCode > -1)
            startActivityForResult(intent, requestCode, options)
        else
            startActivity(intent, options)
    } else {
        if (requestCode > -1)
            startActivityForResult(intent, requestCode)
        else
            startActivity(intent)
    }
}

inline fun <reified T : Any> Context.startActivity(
        options: Bundle? = null,
        noinline init: Intent.() -> Unit = {}) {

    val intent = newIntent<T>(this)
    intent.init()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        startActivity(intent, options)
    } else {
        startActivity(intent)
    }
}

inline fun <reified T : Any> newIntent(context: Context): Intent =
        Intent(context, T::class.java)

inline fun <reified T : ViewDataBinding> Activity.setBindingContentView(@LayoutRes resId: Int): T {
    return DataBindingUtil.setContentView(this, resId)
}