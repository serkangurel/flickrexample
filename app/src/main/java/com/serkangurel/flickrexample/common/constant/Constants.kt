package com.serkangurel.flickrexample.common.constant

object Constants {
    const val BASE_URL = "https://www.flickr.com/services/rest/"
    const val METHOD_GET_RECENT_PHOTOS = "?method=flickr.photos.getRecent"
    const val API_KEY = "576f5e0b045a5dbd978fef8ee5e1d820"
    const val API_RESPONSE_FORMAT = "json"
    const val EXTRAS = "date_taken,url_m,url_l,url_o"
    const val PHOTO_COUNT = 20
}