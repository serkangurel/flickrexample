package com.serkangurel.flickrexample.common.annotation

import androidx.annotation.LayoutRes
import androidx.annotation.TransitionRes

@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
annotation class SetLayout(

        @LayoutRes
        val value: Int,

        @TransitionRes
        val transition: Int = 0,

        val screenName: String = ""

)