package com.serkangurel.flickrexample.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.serkangurel.flickrexample.common.annotation.SetLayout
import com.serkangurel.flickrexample.common.extensions.getAnnotation

abstract class BaseFragment : Fragment() {

    private lateinit var binding: ViewDataBinding

    private var setLayout: SetLayout? = null
    private var screenName: String? = null
    var isDataLoaded = false

    init {
        setLayout = getAnnotation(true) {
            screenName = it.screenName
            if (it.value == 0)
                throw RuntimeException("@SetLayout has invalid value " + javaClass.name)
        }
    }

    @Deprecated("")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, setLayout!!.value, container, false)
        return binding.root
    }

    @Suppress("UNCHECKED_CAST")
    protected fun <T : ViewDataBinding> getBinding(): T {
        return binding as T
    }

    override fun onResume() {
        super.onResume()
        if (userVisibleHint) {
            pageVisible()
        }
    }

    override fun setUserVisibleHint(visible: Boolean) {
        super.setUserVisibleHint(visible)
        if (visible && isResumed && userVisibleHint) {
            pageVisible()
        }
    }

    open fun pageVisible() {
        sendScreenName(screenName)
        if (!isDataLoaded) {
            isDataLoaded = true
            loadData()
        }
    }

    open fun loadData() {

    }

    open fun sendScreenName(screenName: String?) {
        if (!screenName.isNullOrEmpty()) {
            //println(screenName)
        }
    }
}
