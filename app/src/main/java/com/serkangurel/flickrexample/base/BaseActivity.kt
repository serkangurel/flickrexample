package com.serkangurel.flickrexample.base

import android.os.Bundle
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.serkangurel.flickrexample.common.annotation.EnterAnim
import com.serkangurel.flickrexample.common.annotation.ExitAnim
import com.serkangurel.flickrexample.common.annotation.SetLayout
import com.serkangurel.flickrexample.common.extensions.getAnnotation
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

abstract class BaseActivity : AppCompatActivity(), CoroutineScope {

    private lateinit var binding: ViewDataBinding

    private var setLayout: SetLayout? = null
    private var screenName: String? = null

    private var enterAnim: EnterAnim? = null
    private var exitAnim: ExitAnim? = null

    private lateinit var job: Job

    override val coroutineContext: CoroutineContext
        get() = job + Dispatchers.Main //job running in main thread

    init {
        setLayout = getAnnotation(true) {
            screenName = it.screenName
            if (it.value == 0)
                throw RuntimeException("@SetLayout has invalid value " + javaClass.name)
        }
        enterAnim = getAnnotation()
        exitAnim = getAnnotation()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        enterAnim?.let { overridePendingTransition(it.enterAnim, it.exitAnim) }
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, setLayout!!.value)
        binding.lifecycleOwner = this
        job = Job()
    }

    override fun onDestroy() {
        super.onDestroy()
        job.cancel()
    }

    @Suppress("UNCHECKED_CAST")
    protected fun <T : ViewDataBinding> getBinding(): T {
        return binding as T
    }


    fun replaceFragment(fragment: Fragment, @IdRes containerId: Int) {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(containerId, fragment, fragment.javaClass.simpleName)
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun finish() {
        super.finish()
        exitAnim?.let { overridePendingTransition(it.enterAnim, it.exitAnim) }
    }

}
