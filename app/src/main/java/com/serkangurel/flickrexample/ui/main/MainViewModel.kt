package com.serkangurel.flickrexample.ui.main

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.serkangurel.flickrexample.base.BaseViewModel
import com.serkangurel.flickrexample.common.constant.Constants
import com.serkangurel.flickrexample.common.extensions.plusAssign
import com.serkangurel.flickrexample.data.model.RecentPhotos
import com.serkangurel.flickrexample.data.services.RecentPhotosApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainViewModel(
    application: Application,
    private val recentPhotosApiService: RecentPhotosApiService
) :
    BaseViewModel(application) {

    companion object {
        private const val TAG = "MainViewModel"
    }

    private var _recentPhotosLiveData = MutableLiveData<RecentPhotos>()
    val recentPhotosLiveData: LiveData<RecentPhotos>
        get() = _recentPhotosLiveData

    private var _isErrorOccurred = MutableLiveData<Boolean>()
    val isErrorOccurred: LiveData<Boolean>
        get() = _isErrorOccurred

    fun fetchRecentPhotos(page: Int) {
        disposable += recentPhotosApiService.getRecentPhotos(page, Constants.PHOTO_COUNT)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(this::handleResponse, this::handleError)
    }

    private fun handleResponse(recentPhotos: RecentPhotos) {
        _recentPhotosLiveData.postValue(recentPhotos)
    }

    private fun handleError(error: Throwable) {
        Log.e(TAG, "handleError: ${error.message}")
        _isErrorOccurred.postValue(true)
    }

}