package com.serkangurel.flickrexample.ui.photo

import android.content.Context
import android.os.Bundle
import com.bumptech.glide.Glide
import com.serkangurel.flickrexample.R
import com.serkangurel.flickrexample.base.BaseActivity
import com.serkangurel.flickrexample.common.annotation.SetLayout
import com.serkangurel.flickrexample.common.extensions.startActivity
import com.serkangurel.flickrexample.data.model.Photo
import com.serkangurel.flickrexample.databinding.ActivityPhotoBinding

@SetLayout(R.layout.activity_photo)
class PhotoActivity : BaseActivity() {

    private lateinit var binding: ActivityPhotoBinding
    private val photo: Photo by lazy { intent.getParcelableExtra<Photo>(ITEM_PHOTO) }

    companion object {

        const val ITEM_PHOTO = "item_photo"

        fun start(context: Context?, photo: Photo) {
            context?.startActivity<PhotoActivity> {
                putExtra(ITEM_PHOTO, photo)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getBinding()

        Glide.with(this)
            .load(photo.urlL)
            .into(binding.ivPhoto)

        binding.imgBack.setOnClickListener { onBackPressed() }
        binding.tvTitle.text = photo.title

    }
}