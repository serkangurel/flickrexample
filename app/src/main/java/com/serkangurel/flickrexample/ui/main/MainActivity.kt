package com.serkangurel.flickrexample.ui.main

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.serkangurel.flickrexample.R
import com.serkangurel.flickrexample.adapter.CustomErrorItem
import com.serkangurel.flickrexample.adapter.OnRepeatClickListener
import com.serkangurel.flickrexample.adapter.PhotoRVA
import com.serkangurel.flickrexample.base.BaseActivity
import com.serkangurel.flickrexample.common.annotation.SetLayout
import com.serkangurel.flickrexample.common.extensions.observe
import com.serkangurel.flickrexample.databinding.ActivityMainBinding
import org.koin.android.ext.android.inject
import ru.alexbykov.nopaginate.paginate.NoPaginate

@SetLayout(R.layout.activity_main)
class MainActivity : BaseActivity(), OnRepeatClickListener {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by inject()
    private val adaper = PhotoRVA(this)

    private var currentPage = 1
    private lateinit var noPaginate: NoPaginate
    private var isLoadMoreEnabled = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getBinding()
        binding.viewModel = viewModel

        viewModel.fetchRecentPhotos(currentPage)

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.reyclerview.layoutManager = layoutManager
        binding.reyclerview.adapter = adaper

        noPaginate = NoPaginate.with(binding.reyclerview)
            .setOnLoadMoreListener {
                if (isLoadMoreEnabled) {
                    viewModel.fetchRecentPhotos(currentPage)
                    isLoadMoreEnabled = false
                }
            }
            .setCustomErrorItem(CustomErrorItem(this))
            .build()

        observe(viewModel.recentPhotosLiveData) { recentPhotos ->
            val photoList = recentPhotos?.photos?.photoList
            adaper.addList(photoList)

            currentPage++
            isLoadMoreEnabled = true
            noPaginate.showLoading(false)
            noPaginate.showError(false)
        }

        observe(viewModel.isErrorOccurred) {
            if (it == true) {
                noPaginate.showError(true)
            }
        }
    }

    override fun onRepeatClick() {
        viewModel.fetchRecentPhotos(currentPage)
    }

    override fun onDestroy() {
        noPaginate.unbind()
        super.onDestroy()
    }
}