package com.serkangurel.flickrexample.app

import com.google.gson.GsonBuilder
import com.serkangurel.flickrexample.common.constant.Constants
import com.serkangurel.flickrexample.data.services.RecentPhotosApiService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


object NetworkModule {

    //www.flickr.com/services/rest/?method=flickr.photos.getRecent&api_key=576f5e0b045a5dbd978fef8ee5e1d820&per_page=&format=json

    val networkModules = module {
        single { provideRequestInterceptor() }
        single { provideDefaultOkhttpClient(get()) }
        single { provideRetrofit(get()) }
        single { provideService(get()) }
    }

    private fun provideRequestInterceptor(): Interceptor {
        return Interceptor { chain ->
            val url = chain.request()
                .url()
                .newBuilder()
                .addQueryParameter("api_key", Constants.API_KEY)
                .addQueryParameter("format", Constants.API_RESPONSE_FORMAT)
                .addQueryParameter("extras", Constants.EXTRAS)
                .addQueryParameter("nojsoncallback", "1")
                .build()
            val request = chain.request()
                .newBuilder()
                .url(url)
                .build()
            return@Interceptor chain.proceed(request)
        }
    }

    private fun provideDefaultOkhttpClient(
        interceptor: Interceptor
    ): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
    }

    private fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(Constants.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .build()
    }

    private fun provideService(retrofit: Retrofit): RecentPhotosApiService {
        return retrofit.create(RecentPhotosApiService::class.java)
    }

}