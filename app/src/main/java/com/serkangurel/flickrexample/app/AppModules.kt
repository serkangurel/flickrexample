package com.serkangurel.flickrexample.app

import com.serkangurel.flickrexample.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

object AppModules {

    val appModules: Module = module {
        viewModel { MainViewModel(get(), get()) }
    }
}