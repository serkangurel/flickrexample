package com.serkangurel.flickrexample.data.model

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class Photos(
    @SerializedName("page")
    val page: Int?,
    @SerializedName("pages")
    val pages: Int?,
    @SerializedName("perpage")
    val perpage: Int?,
    @SerializedName("photo")
    val photoList: List<Photo>?,
    @SerializedName("total")
    val total: Int?
) : Parcelable