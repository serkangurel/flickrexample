package com.serkangurel.flickrexample.data.model

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import android.os.Parcelable

@Parcelize
data class RecentPhotos(
    @SerializedName("photos")
    val photos: Photos?,
    @SerializedName("stat")
    val stat: String?
): Parcelable