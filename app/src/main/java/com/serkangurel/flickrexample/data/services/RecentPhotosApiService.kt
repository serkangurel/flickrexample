package com.serkangurel.flickrexample.data.services

import com.serkangurel.flickrexample.common.constant.Constants
import com.serkangurel.flickrexample.data.model.RecentPhotos
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RecentPhotosApiService {

    @GET(Constants.METHOD_GET_RECENT_PHOTOS)
    fun getRecentPhotos(
        @Query("page") page: Int,
        @Query("per_page") photoCount: Int
    ): Observable<RecentPhotos>
}