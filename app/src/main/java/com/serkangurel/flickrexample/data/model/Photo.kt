package com.serkangurel.flickrexample.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Photo(
    @SerializedName("farm")
    val farm: Int?,
    @SerializedName("height_l")
    val heightL: Int?,
    @SerializedName("height_m")
    val heightM: Int?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("isfamily")
    val isfamily: Int?,
    @SerializedName("isfriend")
    val isfriend: Int?,
    @SerializedName("ispublic")
    val ispublic: Int?,
    @SerializedName("owner")
    val owner: String?,
    @SerializedName("secret")
    val secret: String?,
    @SerializedName("server")
    val server: String?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("datetaken")
    val date: String?,
    @SerializedName("url_l")
    val urlL: String?,
    @SerializedName("url_m")
    val urlM: String?,
    @SerializedName("url_o")
    val urlO: String?,
    @SerializedName("width_l")
    val widthL: Int?,
    @SerializedName("width_m")
    val widthM: Int?
) : Parcelable